set -ex
yum -y install fftw-devel netcdf-cxx-devel
curl -L https://github.com/dschwoerer/multimpi/releases/download/v0.1/multimpi-0.1.tar.xz > multimpi.tar.xz
tar -xvf multimpi.tar.xz -C /usr/local
MPIPATH=/usr/local/multimpi
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MPIPATH/lib/
./configure --enable-shared  CC=$MPIPATH/bin/mpicc CXX=$MPIPATH/bin/mpic++
make -j 4
cd tools/pylib/_boutcore_build/

export NAME=boutcore

export PLAT=$1

function repair_wheel {
    wheel="$1"
    if ! auditwheel show "$wheel"; then
        echo "Skipping non-platform wheel $wheel"
    else
        auditwheel repair "$wheel" --plat "$PLAT" -w /io/wheelhouse/
    fi
}

for PYBIN in /opt/python/cp3*/bin;
do
    make clean
    npv=1.15
    test $($PYBIN/python -V |grep 3.. -o) == 3.9 && npv=1.18
    $PYBIN/pip install numpy==$npv cython
    PY=$PYBIN/python make
    "${PYBIN}/pip" wheel . --no-deps -w wheelhouse/
done

# Build sdist
$PYBIN/python setup.py sdist || :

for whl in wheelhouse/*.whl; do
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/../../../lib/ repair_wheel "$whl"
done

for PYBIN in /opt/python/cp3*/bin/; do
    "${PYBIN}/pip" install $NAME --no-index -f /io/wheelhouse
    if test -e "${PYBIN}/nosetests"
    then
	(
	    cd "$HOME"
	    "${PYBIN}/nosetests" $NAME
	)
    fi
done
rm -f wheelhouse/*

yum -y install zip
mkdir -p tmp
cd tmp/
for whl in $(ls /io/wheelhouse/) ; do
    rm * -rf
    unzip /io/wheelhouse/$whl
    find
    cp $MPIPATH/lib/*so boutcore.libs/
    zip ../wheelhouse/$whl * */*
    ls -l ../wheelhouse/
done
cd ..


$PYBIN/pip install twine --upgrade
ls -l $PYBIN
$PYBIN/python -m twine upload --repository testpypi wheelhouse/*.whl
